package main

import (
	"context"
	"errors"
	"fmt"
	"io"
	"log"
	"net"
	"os"
	"reflect"

	"github.com/spf13/cobra"
	. "gitlab.com/ethan.reesor/vscode-notebooks/go-playbooks/pkg/kernel"
	"gitlab.com/ethan.reesor/vscode-notebooks/yaegi/interp"
)

func main() {
	_ = cmd.Execute()
}

var cmd = &cobra.Command{
	Use:  "go-playbook-kernel [socket file]",
	Args: cobra.ExactArgs(1),
	Run:  run,
}

func check(err error) {
	if err == nil {
		return
	}

	if errors.Is(err, io.EOF) {
		return
	}

	log.Println(err)
	os.Exit(1)
}

func run(_ *cobra.Command, args []string) {
	// Create the connection
	conn, err := net.Dial("unix", args[0])
	check(err)
	kernel := New(context.Background(), conn, interp.Options{})

	check(kernel.Use(interp.Exports{
		"notebook/notebook": map[string]reflect.Value{
			"Abort": reflect.ValueOf(func(v ...interface{}) { kernel.Abort(v...) }),
			"Output": reflect.ValueOf(func(v ...interface{}) {
				event := kernel.Output()

				for len(v) > 0 {
					if len(v) == 1 {
						event = event.Add("text/plain", []byte(fmt.Sprint(v[0])))
						break
					}

					mime := fmt.Sprint(v[0])
					switch value := v[1].(type) {
					case string:
						event = event.Add(mime, []byte(value))
					case []byte:
						event = event.Add(mime, value)
					case fmt.Stringer:
						event = event.Add(mime, []byte(value.String()))
					case io.Reader:
						b, err := io.ReadAll(value)
						if err == nil {
							event = event.Add(mime, b)
						} else {
							event = event.Add(mime, []byte(fmt.Sprint(value)))
						}
					default:
						event = event.Add(mime, []byte(fmt.Sprint(value)))
					}
					v = v[2:]
				}

				kernel.SendSafe(event)
			}),
		},
	}))

	_, err = kernel.Eval(
		`import (
			"fmt"
			"notebook"
		)`,
	)
	check(err)

	// Handle incoming events
	rd := NewEventReader(conn)
	for {
		event, err := rd.Read()
		if err != nil {
			check(err)
			break
		}

		if !kernel.Event(event) {
			break
		}
	}

	check(kernel.Err())
}
