# Go Playbooks for VSCode

> Powered by [Yaegi](https://github.com/traefik/yaegi)

A Visual Studio Code extension for Go notebooks.

![hello world](images/hello.png)

## Notebook Format

Go Playbooks supports `*.play` notebooks written in Markdown. The parser is a
fork of Microsoft's [vscode-markdown-notebook][ms-notebook] project. In addition
to the basic Markdown notebook features provided by Microsoft's implementation,
Go Playbooks adds support for frontmatter and output cells.

[ms-notebook]: https://github.com/microsoft/vscode-markdown-notebook

## Notebook API

The default kernel defines the `notebook` package which exposes the following:

```go
package notebook

// Output produces an output cell. It produces an output item for every two
// inputs. In the simplest usage, Output(mime, value), it produces a single item
// with the specified mime type and value. If Output is called with an odd
// number of inputs, the last unpaired input is treated as a text/plain value.
//
// If the value is a string or byte slice, it is used unchanged. If it is a
// Stringer, its String method is called. If it is a reader, the result of
// io.ReadAll is used. Otherwise, fmt.Sprint is used to convert the value to a
// string.
func Output(...interface{})
```

## Installing the Kernel

If `go-playbook-kernel` is not already installed, the first time a Go notebook
cell is executed Go Playbooks prompts the user to install the kernel if the Go
extension is installed. If the Go extension is not installed, the user must
manually install the kernel and put it on the path.

![not-found](images/not-found.png)

## Alternate Kernels

Go Playbooks allows the use of alternate kernels. If the frontmatter specifies
an engine, Go Playbooks will look for the named executable on the path. If the
executable cannot be found, Go Playbooks will attempt to use the Go extension to
locate and/or install it. The engine specification must conform to the
following:

```typescript
interface Tool {
  name: string;        // The executable name
  description: string; // A label used to describe the kernel
  importPath: string;  // The import path of the executable
  modulePath: string;  // The path of the Go module containing the import path
  version?: string;    // The version of the module to use
}
```

For example:

```yaml
engine:
  name: custom-kernel
  description: Example Custom Kernel
  importPath: example.com/playbooks/cmd/custom-kernel
  modulePath: example.com/playbooks
  version: v1.2.3
```

### Debugging

If the Go extension is installed and  the engine specification enables debugging
and specifies a source location, Go Playbooks will launch the kernel as a debug
session. For example:

```yaml
engine:
  debug:
    enable: true
    source: /path/to/cmd/go-playbook-kernel
```

The kernel source is not distributed with the extension. Debugging the default
kernel requires cloning the extension repository.
