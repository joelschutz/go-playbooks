module gitlab.com/ethan.reesor/vscode-notebooks/go-playbooks

go 1.18

require (
	github.com/spf13/cobra v1.4.0
	gitlab.com/ethan.reesor/vscode-notebooks/yaegi v0.0.0-20220417214422-5c573557938e
)

require (
	github.com/inconshreveable/mousetrap v1.0.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
)
