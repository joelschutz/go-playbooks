/*---------------------------------------------------------------------------------------------
 *  Copyright (c) Microsoft Corporation. All rights reserved.
 *  Licensed under the MIT License. See LICENSE in this folder for license information.
 *--------------------------------------------------------------------------------------------*/

import * as vscode from 'vscode';
import { parseMarkdown, writeCellsToMarkdown, RawNotebookCell } from './markdownParser';

const providerOptions = {
	transientCellMetadata: {
		runnable: true,
		editable: true,
		custom: true,
		rawMetadata: true,
	},
	transientOutputs: false
};

export function activate(context: vscode.ExtensionContext, notebookType: string) {
	context.subscriptions.push(vscode.workspace.registerNotebookSerializer(notebookType, new MarkdownProvider(), providerOptions));
}

// there are globals in workers and nodejs
declare class TextDecoder {
	decode(data: Uint8Array): string;
}
declare class TextEncoder {
	encode(data: string): Uint8Array;
}

class MarkdownProvider implements vscode.NotebookSerializer {

	private readonly decoder = new TextDecoder();
	private readonly encoder = new TextEncoder();

	deserializeNotebook(data: Uint8Array, _token: vscode.CancellationToken): vscode.NotebookData | Thenable<vscode.NotebookData> {
		const content = this.decoder.decode(data);
		const cells = parseMarkdown(content).map(rawToNotebookCellData);
		return { cells };
	}

	serializeNotebook(data: vscode.NotebookData, _token: vscode.CancellationToken): Uint8Array | Thenable<Uint8Array> {
		const stringOutput = writeCellsToMarkdown(data.cells);
		return this.encoder.encode(stringOutput);
	}
}

function rawToNotebookCellData(data: RawNotebookCell): vscode.NotebookCellData {
	const { kind, language, content, output, ...metadata } = data;
	const cell = <vscode.NotebookCellData>{
		kind: kind,
		languageId: language,
		metadata: metadata,
		outputs: [],
		value: content
	};

	for (const raw of output || []) {
		rawToNotebookCellOutput(cell, raw);
	}

	return cell;
}

function rawToNotebookCellOutput(cell: vscode.NotebookCellData, data: RawNotebookCell) {
	const { kind, language, content, ...metadata } = data;
	const { id = 0, mime = 'text/plain' } = metadata.metadata || {};
	const item = new vscode.NotebookCellOutputItem(Buffer.from(content, 'utf-8'), mime);

	for (const output of cell.outputs!) {
		if (output.metadata?.id === id) {
			output.items.push(item);
			return;
		}
	}

	const output = new vscode.NotebookCellOutput([item], { id });
	cell.outputs!.push(output);
}