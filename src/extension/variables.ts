import { commands, Disposable, EventEmitter, ExtensionContext, ProviderResult, TreeDataProvider, TreeItem, TreeItemCollapsibleState, Uri, window, workspace } from "vscode"
import { NotebookKernel, NotebookKernelFactory, Variable } from "./kernel"

export class VariablesProvider implements TreeDataProvider<Variable> {
    private readonly didChangeTreeData = new EventEmitter<void>()
    public readonly onDidChangeTreeData = this.didChangeTreeData.event

    private kernel: NotebookKernel | undefined
    private subOnDidExecute: Disposable | undefined

    private readonly disposable: Disposable[] = []

    constructor(factory: NotebookKernelFactory) {
        this.disposable.push(factory.onDidChangeActiveKernel(kernel => {
            this.subOnDidExecute?.dispose()
            this.kernel = kernel
            this.subOnDidExecute = kernel?.onDidExecute(() => this.didChangeTreeData.fire())
            this.didChangeTreeData.fire()
        }))
    }

    dispose() {
        this.subOnDidExecute?.dispose()
        this.disposable.forEach(x => x.dispose())
    }

    async getChildren(v?: Variable): Promise<Variable[]> {
        const { kernel } = this
        if (!kernel) {
            return []
        }

        if (!v) {
            const r = await kernel.getVariables()
            return r
        }

        if (!v.reference) {
            return []
        }

        return await kernel.getVariables(v.reference)
    }

    getTreeItem(v: Variable): TreeItem {
        return {
            label: `${v.name}: ${v.value}`,
            id: v.reference?.toString(),
            collapsibleState: v.reference ? TreeItemCollapsibleState.Collapsed : TreeItemCollapsibleState.None,
        }
    }
}