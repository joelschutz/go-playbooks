import vscode = require('vscode')
import { activate as activateNotebook } from '../vscode-markdown-notebook/extension'
import { NotebookKernel, NotebookKernelFactory } from './kernel'
import { UnixSocketProcess } from './stream'
import { VariablesProvider } from './variables'

export class Resources {
	private static readonly resources = new Set<vscode.Disposable>()

	static register(r: vscode.Disposable) {
		this.resources.add(r)
	}

	static unregister(r: vscode.Disposable) {
		this.resources.delete(r)
	}

	static async dispose() {
		for (const r of this.resources) {
			await r.dispose()
		}
	}
}

export function activate(context: vscode.ExtensionContext) {
	UnixSocketProcess.extensionPath = context.extensionPath

	activateNotebook(context, 'go-playbooks')

	if (vscode.workspace.isTrusted) {
		enableKernel(context)
	} else {
		context.subscriptions.push(vscode.workspace.onDidGrantWorkspaceTrust(() => enableKernel(context)))
	}
}


function enableKernel(context: vscode.ExtensionContext) {
	const factory = new NotebookKernelFactory()
	const variables = new VariablesProvider(factory)
	context.subscriptions.push(factory, variables)
	context.subscriptions.push(vscode.window.registerTreeDataProvider('variables', variables))

	context.subscriptions.push(vscode.commands.registerCommand('go-playbooks.stop-active-kernel', stopKernel))
	context.subscriptions.push(vscode.commands.registerCommand('go-playbooks.editor.stop-active-kernel', stopKernel))

	const controller = vscode.notebooks.createNotebookController('go-playbooks', 'go-playbooks', 'Go Playbook')
	controller.supportedLanguages = ['go']
	controller.supportsExecutionOrder = true
	controller.interruptHandler = async (doc) => {
		const kernel = await factory.for(doc, false)
		await kernel?.interrupt()
	}
	controller.executeHandler = async (cells, doc, ctrl) => {
		const kernel = await factory.for(doc)
		await kernel?.execute(cells, doc, ctrl)
	}
	context.subscriptions.push(controller)

	async function stopKernel(event?: { notebookEditor: { notebookUri: vscode.Uri } }) {
		const active = vscode.window.activeTextEditor?.document
		const uri =
			event ? event.notebookEditor.notebookUri :
			active ? active.uri :
			null
		if (!uri) {
			return
		}

		const kernel = await factory.forUri(uri, false)
		if (!kernel) {
			return
		}

		kernel.dispose()
	}
}

export async function deactivate() {
	await Resources.dispose()
}
