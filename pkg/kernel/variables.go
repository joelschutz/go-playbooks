package kernel

import (
	"fmt"
	"reflect"
	"sync"
)

type variables struct {
	mu     *sync.Mutex
	values []variableScope
	id     int
}

func newVariables() *variables {
	v := new(variables)
	v.mu = new(sync.Mutex)
	return v
}

func (r *variables) Purge() {
	r.mu.Lock()
	defer r.mu.Unlock()

	r.id = 0
	if r.values != nil {
		r.values = r.values[:0]
	}
}

func (r *variables) Add(v variableScope) int {
	r.mu.Lock()
	defer r.mu.Unlock()

	r.id++
	r.values = append(r.values, v)
	return r.id
}

func (r *variables) Get(i int) (variableScope, bool) {
	r.mu.Lock()
	defer r.mu.Unlock()

	if i < 1 || i > len(r.values) {
		return nil, false
	}
	return r.values[i-1], true
}

func (a *Kernel) newVar(name string, rv reflect.Value) Variable {
	var v Variable
	v.Name = name

	if canBeNil(rv.Kind()) && rv.IsNil() {
		v.Value = "nil"
		return v
	}

	switch rv.Kind() {
	case reflect.Chan, reflect.Func, reflect.Interface, reflect.Map, reflect.Ptr, reflect.Slice, reflect.Array, reflect.Struct:
		v.Value = fmt.Sprint(rv.Type())
	default:
		v.Value = fmt.Sprint(rv)
	}

	switch rv.Kind() {
	case reflect.Interface, reflect.Ptr:
		v.Reference = a.vars.Add(&elemVars{rv})
	case reflect.Array, reflect.Slice:
		v.Reference = a.vars.Add(&arrayVars{rv})
	case reflect.Struct:
		v.Reference = a.vars.Add(&structVars{rv})
	case reflect.Map:
		v.Reference = a.vars.Add(&mapVars{rv})
	}

	return v
}

func canBeNil(k reflect.Kind) bool {
	return k == reflect.Chan || k == reflect.Func || k == reflect.Interface || k == reflect.Map || k == reflect.Ptr || k == reflect.Slice
}

type variableScope interface {
	Variables(*Kernel) []Variable
}

type elemVars struct {
	reflect.Value
}

func (v *elemVars) Variables(k *Kernel) []Variable {
	return []Variable{k.newVar("", v.Elem())}
}

type arrayVars struct {
	reflect.Value
}

func (v *arrayVars) Variables(k *Kernel) []Variable {
	vars := make([]Variable, v.Len())
	for i := range vars {
		vars[i] = k.newVar(fmt.Sprint(i), v.Index(i))
	}
	return vars
}

type structVars struct {
	reflect.Value
}

func (v *structVars) Variables(k *Kernel) []Variable {
	vars := make([]Variable, v.NumField())
	typ := v.Type()
	for i := range vars {
		f := typ.Field(i)
		name := f.Name
		if name == "" {
			name = f.Type.Name()
		}
		vars[i] = k.newVar(name, v.Field(i))
	}
	return vars
}

type mapVars struct {
	reflect.Value
}

func (v *mapVars) Variables(n *Kernel) []Variable {
	keys := v.MapKeys()
	vars := make([]Variable, len(keys))
	for k, y := range keys {
		vars[k] = n.newVar(fmt.Sprint(y), v.MapIndex(y))
	}
	return vars
}
