package kernel

import (
	"encoding/json"
	"sort"
	"time"
)

type NotebookCapabilities struct {
	CanAbort bool
}

type InterruptRequest struct{}

type ExecuteRequest struct {
	Id   int
	Code string
}

type VariableRequest struct {
	Id        int
	Reference int
}

func UnmarshalEvent(data []byte) (interface{}, error) {
	var typ struct{ Type string }
	err := json.Unmarshal(data, &typ)
	if err != nil {
		return nil, err
	}

	var v interface{}
	switch typ.Type {
	case "capabilities":
		v = new(NotebookCapabilities)
	case "interrupt":
		v = new(InterruptRequest)
	case "execute":
		v = new(ExecuteRequest)
	case "variable":
		v = new(VariableRequest)
	default:
		v = map[string]interface{}{}
	}

	err = json.Unmarshal(data, &v)
	if err != nil {
		return nil, err
	}

	return v, nil
}

type CapabilitiesEvent struct {
	CanProvideVariables bool `json:"canProvideVariables"`
}

func (e CapabilitiesEvent) MarshalJSON() ([]byte, error) {
	type event CapabilitiesEvent
	return marshalWithType("capabilities", event(e))
}

type ExecutedEvent struct {
	ID         int             `json:"id"`
	Success    bool            `json:"success"`
	Result     json.RawMessage `json:"result"`
	Duration   float64         `json:"duration"`
	ShowResult bool            `json:"showResult"`
}

func (e ExecutedEvent) MarshalJSON() ([]byte, error) {
	type event ExecutedEvent
	return marshalWithType("executed", event(e))
}

func Executed(id int, success bool, duration time.Duration, result json.RawMessage, showResult bool) ExecutedEvent {
	return ExecutedEvent{
		ID:         id,
		Success:    success,
		Duration:   duration.Seconds(),
		Result:     result,
		ShowResult: showResult,
	}
}

type VariablesResponse struct {
	ID        int        `json:"id"`
	Error     string     `json:"error,omitempty"`
	Variables []Variable `json:"variables"`
}

type Variable struct {
	Reference int    `json:"reference,omitempty"`
	Name      string `json:"name"`
	Value     string `json:"value"`
}

func Variables(id int, vars []Variable) VariablesResponse {
	return VariablesResponse{ID: id, Variables: vars}
}

func VariablesError(id int, err error) VariablesResponse {
	return VariablesResponse{ID: id, Error: err.Error()}
}

func (e VariablesResponse) Sort() {
	sort.Slice(e.Variables, func(i, j int) bool {
		return e.Variables[i].Name < e.Variables[j].Name
	})
}

func (e VariablesResponse) MarshalJSON() ([]byte, error) {
	type event VariablesResponse
	return marshalWithType("variables", event(e))
}

type OutputEvent struct {
	ID    int          `json:"id"`
	Items []OutputItem `json:"items"`
}

type OutputItem struct {
	Mime  string `json:"mime"`
	Value []byte `json:"value"`
}

func Output(id int) OutputEvent {
	return OutputEvent{ID: id}
}

func (e OutputEvent) MarshalJSON() ([]byte, error) {
	type event OutputEvent
	return marshalWithType("output", event(e))
}

func (e OutputEvent) Add(mime string, value []byte) OutputEvent {
	e.Items = append(e.Items, OutputItem{mime, value})
	return e
}

type AbortEvent struct {
	ID     int             `json:"id"`
	Value  json.RawMessage `json:"value,omitempty"`
	Line   int             `json:"line,omitempty"`
	Column int             `json:"column,omitempty"`
	Stack  string          `json:"stack,omitempty"`
}

func (e AbortEvent) MarshalJSON() ([]byte, error) {
	type event AbortEvent
	return marshalWithType("abort", event(e))
}

func Abort(id int, value ...interface{}) AbortEvent {
	switch len(value) {
	case 0:
		return AbortEvent{ID: id}
	case 1:
		b, _ := json.Marshal(value[0])
		return AbortEvent{ID: id, Value: b}
	default:
		b, _ := json.Marshal(value)
		return AbortEvent{ID: id, Value: b}
	}
}

func (e AbortEvent) WithLine(line, column int) AbortEvent {
	e.Line = line
	e.Column = column
	return e
}

func (e AbortEvent) WithStack(stack string) AbortEvent {
	e.Stack = stack
	return e
}

type ErrorEvent struct {
	ID      int    `json:"id"`
	Name    string `json:"name,omitempty"`
	Message string `json:"message,omitempty"`
	Line    int    `json:"line,omitempty"`
	Column  int    `json:"column,omitempty"`
	Stack   string `json:"stack,omitempty"`
}

func (e ErrorEvent) MarshalJSON() ([]byte, error) {
	type event ErrorEvent
	return marshalWithType("error", event(e))
}

func Error(id int, message string) ErrorEvent {
	return ErrorEvent{ID: id, Message: message}
}

func (e ErrorEvent) WithName(name string) ErrorEvent {
	e.Name = name
	return e
}

func (e ErrorEvent) WithLine(line, column int) ErrorEvent {
	e.Line = line
	e.Column = column
	return e
}

func (e ErrorEvent) WithStack(stack string) ErrorEvent {
	e.Stack = stack
	return e
}

func marshalWithType(typ string, v interface{}) ([]byte, error) {
	b, err := json.Marshal(v)
	if err != nil {
		return nil, err
	}
	var m map[string]interface{}
	err = json.Unmarshal(b, &m)
	if err != nil {
		return nil, err
	}
	m["type"] = typ
	return json.Marshal(m)
}
