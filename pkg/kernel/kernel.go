package kernel

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"reflect"
	"runtime/debug"
	"strings"
	"time"

	"gitlab.com/ethan.reesor/vscode-notebooks/yaegi/interp"
	"gitlab.com/ethan.reesor/vscode-notebooks/yaegi/stdlib"
)

func New(ctx context.Context, wr io.Writer, opts interp.Options) *Kernel {
	kernel := &Kernel{wr: wr}
	kernel.vars = newVariables()
	kernel.context, kernel.cancel = context.WithCancel(ctx)

	// Set up IO
	rout, wout := io.Pipe()
	rerr, werr := io.Pipe()
	go kernel.sendOut(rout, "stdout")
	go kernel.sendOut(rerr, "stderr")

	// Set up the kernel
	if opts.Stdin == nil {
		opts.Stdin = new(bytes.Buffer)
	}
	opts.Stdout, opts.Stderr = wout, werr
	kernel.stdout, kernel.stderr = wout, werr
	kernel.interp = interp.New(opts)

	_ = kernel.interp.Use(stdlib.Symbols)
	return kernel
}

type Kernel struct {
	wr      io.Writer
	stdout  io.Writer
	stderr  io.Writer
	context context.Context
	cancel  context.CancelFunc
	errs    []error
	interp  *interp.Interpreter
	current *session
	vars    *variables

	Notebook NotebookCapabilities
}

type session struct {
	kernel     *Kernel
	id         int
	program    *interp.Program
	showResult bool
	abort      *didAbort
	context    context.Context
	cancel     context.CancelFunc
}

type didAbort struct {
	value []interface{}
	stack []byte
}

func (k *Kernel) Abort(v ...interface{}) {
	if k.current == nil {
		return
	}

	k.current.abort = &didAbort{
		value: v,
		stack: debug.Stack(),
	}
	k.current.cancel()
	panic(k.current.abort)
}

func (k *Kernel) Stdout() io.Writer                      { return k.stdout }
func (k *Kernel) Stderr() io.Writer                      { return k.stderr }
func (k *Kernel) Use(values interp.Exports) error        { return k.interp.Use(values) }
func (k *Kernel) Eval(src string) (reflect.Value, error) { return k.interp.Eval(src) }

func (k *Kernel) Err() error {
	switch len(k.errs) {
	case 0:
		return nil
	case 1:
		return k.errs[0]
	}

	var errs []string
	for _, err := range k.errs {
		errs = append(errs, err.Error())
	}
	return errors.New(strings.Join(errs, "; "))
}

func (k *Kernel) report(err error) {
	if !errors.Is(err, io.EOF) {
		k.errs = append(k.errs, err)
	}
	k.cancel()
}

func (k *Kernel) Send(event interface{}) error {
	return WriteEvent(k.wr, event)
}

func (k *Kernel) Output() OutputEvent {
	if k.current == nil {
		return Output(0)
	}
	return Output(k.current.id)
}

func (k *Kernel) SendSafe(event interface{}) bool {
	err := k.Send(event)
	if err != nil {
		k.report(err)
	}
	return err == nil
}

func (k *Kernel) sendOut(r *io.PipeReader, mime string) {
	var buf [1 << 20]byte
	for {
		n, err := r.Read(buf[:])
		if err != nil {
			// If the script closes stdout/err... ok
			k.errs = append(k.errs, err)
			return
		}

		if !k.SendSafe(k.Output().Add(mime, buf[:n])) {
			return
		}
	}
}

func (k *Kernel) Event(event interface{}) bool {
	// Refuse to process events if anything went wrong
	if len(k.errs) > 0 {
		return false
	}

	switch event := event.(type) {
	case *NotebookCapabilities:
		k.Notebook = *event
		k.SendSafe(CapabilitiesEvent{
			CanProvideVariables: true,
		})

	case *InterruptRequest:
		if k.current != nil {
			k.current.cancel()
		}

	case *ExecuteRequest:
		// Wait until any previous execution is done
		if k.current != nil {
			<-k.current.context.Done()
		}

		s := k.compile(event.Id, event.Code)
		if s == nil {
			break
		}

		// Purge variables once execution is done
		go func() {
			<-k.current.context.Done()
			k.vars.Purge()
		}()

		k.current = s
		go k.current.run()

	case *VariableRequest:
		// Wait until any previous execution is done
		if k.current != nil {
			<-k.current.context.Done()
		}

		if event.Reference == 0 {
			g := k.interp.Globals()
			vars := make([]Variable, 0, len(g))
			kernel := k
			for k, v := range g {
				vars = append(vars, kernel.newVar(fmt.Sprint(k), v))
			}
			k.SendSafe(Variables(event.Id, vars))
			break
		}

		scope, ok := k.vars.Get(event.Reference)
		if !ok {
			k.SendSafe(VariablesError(event.Id, errors.New("unknown variable reference")))
			break
		}

		k.SendSafe(Variables(event.Id, scope.Variables(k)))
	}

	return len(k.errs) == 0
}

func (s *session) run() {
	var success bool
	var duration time.Duration
	var result json.RawMessage
	defer func() { s.kernel.SendSafe(Executed(s.id, success, duration, result, s.showResult)) }()
	defer s.cancel()

	defer func() {
		if r := recover(); r != nil {
			s.kernel.SendSafe(Error(s.id, fmt.Sprint(r)).
				WithName("Panic").
				WithStack(string(debug.Stack())))
		}
	}()

	before := time.Now()
	rv, err := s.kernel.interp.ExecuteWithContext(s.context, s.program)
	after := time.Now()
	duration = after.Sub(before)
	success = err == nil

	if err != nil {
		s.kernel.sendErr(s.id, err, s.abort)
	}

	if !rv.IsValid() || !rv.CanInterface() {
		return
	}

	v := rv.Interface()
	result, _ = json.Marshal(v)

	err, ok := v.(error)
	if ok && string(result) == "{}" {
		result, _ = json.Marshal(map[string]string{
			"error": err.Error(),
		})
	}
}
